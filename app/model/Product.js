Ext.define("MyApp.model.Product",{

	extend:"Ext.data.Model",
	fields:[
		{name:"id",type:"int"},
		{name:"name",type:"string"},
		{name:"price",type:"number"},
		{name:"category"}
	],

	validators:{

		id:[{type:"presence"}],
		name:[{type:"presence"},{type:"length",min:3}]
	}


});