/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('MyApp.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

    routes:{

        "Home":"onHome",
        "Users":"onUsers",
        "Products":"onProducts",
        "Posts":"onPosts",
        "MVC":"onMvc",
        "Products/:id":"onProductsWithId"
    },

    onProductsWithId:function(id){
        var tab = this.getView().down("#products");
        this.getView().setActiveTab(tab);

        var grid = this.getView().down("#products").down("productsgrid");
        grid.selModel.doSelect(
            grid.store.data.items[(id-1)]);
        
    },
    onHome:function(){

   // console.log("onHome");
    var tab = this.getView().down("#home");
    this.getView().setActiveTab(tab);

    },
    onUsers:function(){

    //console.log("onUsers");
    var tab = this.getView().down("#users");
    this.getView().setActiveTab(tab);

    },
    onProducts:function(){

   // console.log("onProducts");
    var tab = this.getView().down("#products");
    this.getView().setActiveTab(tab);
    },
    onPosts:function(){

    //console.log("onPosts");
    var tab = this.getView().down("#post");
    this.getView().setActiveTab(tab);

    },
    onMvc:function(){

    // console.log("onMvc");
     var tab = this.getView().down("#mvc");
    this.getView().setActiveTab(tab);

    },

    onTabClicked:function(tab){
        var title = tab.title;
        this.redirectTo(title);
    },
    onItemSelected: function (sender, record) {
        Ext.Msg.confirm('Confirm', 'Are you sure?', 'onConfirm', this);
    },

    onConfirm: function (choice) {
        if (choice === 'yes') {
            //
        }
    }
});
