Ext.define("MyApp.view.products.ProductController",{
	extend:"Ext.app.ViewController",
	alias:"controller.productcontroller",

	/*onGridSelect:function(grid,record){
		console.log(record);
		this.getViewModel().set("currentProduct",record);

	}*/
//alternative way in grid 
		commit:function(){
		
		this.getViewModel().get("currentProduct").commit();
	},
		reject:function(){
		
		this.getViewModel().get("currentProduct").reject();
	},
	addNew:function(){

		var store = this.getViewModel().getStore("products");
		var record = store.insert(0,{})[0];
		this.getViewModel().set("currentProduct",record);
	},
	commitAll:function(){

		var store = this.getViewModel().getStore("products");
		commitChanges();
	},
	rejectAll:function(){

		var store = this.getViewModel().getStore("products");
		rejectChanges();
	},
	search:function(field){

		// var store = this.getViewModel().getStore("products");
		var store = field.up("panel").getStore();
		var text = field.getValue();
		if(text){
		store.setFilters([
				{property:"name",
				 value:text}
				]);
	}
	else{

		store.clearFilter();
	}
    },
     save: function(field){
		//var store = field.up("panel").getStore();
        var store = this.getViewModel().getStore("products");
        store.save();
    }



});