Ext.define("MyApp.view.products.ProductModel",{
	extend:"Ext.app.ViewModel",
	alias:"viewmodel.productviewmodel",
	requires:[MyApp.store.products],
	data:
	{
		currentProduct:null
	},
	stores:{
			products:{ type:"productstore"},
			chainedStore1:{
				source:"{products}",
				sorters:[
				{property:"name",direction:"ASC"}
				]
			},

			chainedStore2:{
				source:"{products}",
				sorters:[
				{property:"name",direction:"DESC"}
				]
			}
	},

	formulas:{

		isRecordDirty:{
			bind:{
			bindTo:"{currentProduct}",
			deep :true
		},
			get:function (product) {
				//console.log("in dirty");
			//	var product = this.get("currentProduct");
				if(product){
				//console.log("dirty"+product.dirty);
				return product.dirty;
			}
				return false;
			}
		}
	}
		
});
