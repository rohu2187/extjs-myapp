Ext.define("MyApp.view.post.PostController",{
	extend:"Ext.app.ViewController",
	alias:"controller.postcontroller",

     remove: function(ctrl){
		var record = ctrl.getWidgetRecord()
        var store = this.getViewModel().getStore("post");
        store.remove(record);
        store.save(record);
    },

     save: function(ctrl){
		var store = this.getViewModel().getStore("post");
        store.save();
    }


});