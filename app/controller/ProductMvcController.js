Ext.define("MyApp.controller.ProductMvcController",{
	extend:"Ext.app.Controller",
	alias:"controller.productmvccontroller",

	init:function () {
		this.control({
			"mvcproductsgrid":{
				select:this.onSelectProductGrid

			},
			"mvcproductsgrid button[action=New]":{
				click: function(){
					//console.log("new button clicked");
				}
			}
			/*"mvcproductsgrid button[action=Update]":{
				click: this.onFormUpdate
			}*/
		});
		this.listen({
			global:{
				idle:function(){
					/*console.log("app idle");*/
				}
			},
			component:{
				"mvcproductsform button[action=Update]":{
				
				click: this.onFormUpdate
			}
			}
		});
	},
	onSelectProductGrid:function(grid,record){

		//console.log("selected");
		var form = Ext.ComponentQuery.query("mvcproductsform")[0];
		form.loadRecord(record);

	},

    onFormUpdate:function(btn){
        //console.log("invole from listen");
		var form = Ext.ComponentQuery.query("mvcproductsform")[0];
		form.updateRecord();

	}
})