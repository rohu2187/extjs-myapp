Ext.define("MyApp.store.Posts",{

	extend:"Ext.data.Store",
	alias:"store.poststore",
	// model:"Ext.model.Posts",
	autoLoad:true,
	fields:["id","userId","title","body"],
	proxy:{
		type:"rest",
		url:"https://jsonplaceholder.typicode.com/posts"
		
		}
});