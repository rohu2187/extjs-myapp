Ext.define("MyApp.store.Products",{
	extend:"Ext.data.Store",
	alias:"store.productstore",
	//fields:["id","name","price","category"],
	model:"MyApp.model.Product",
autoLoad:true,
storeId: "productstoreId",
	data:[
	{id:"1",name:"Anand",price:85000,category:"Railways"},
	{id:"2",name:"Vishal",price:8500000,category:"Chatting"}
	],

/*	proxy: {
		type: "ajax",
		url: "products.json"
	}*/

	proxy: {
		type:"localstorage",
		id:"products"
	}
})