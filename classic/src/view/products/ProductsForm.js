Ext.define("MyApp.view.product.ProductsForm",{

	extend:"Ext.form.Panel",
	alias:"widget.productsform",
	title:"Form ",
	modelValidation:true,
	config:{
		currentProduct:null
	},
	bind:{
		currentProduct:"{currentProduct}"
	},

	items:[
		{
			xtype:"textfield",
			fieldLabel:"Id",
			bind:"{currentProduct.id}"
		},
		{
			xtype:"textfield",
			fieldLabel:"Name",
			bind:"{currentProduct.name}"
		},
		{
			xtype:"textfield",
			fieldLabel:"Price",
			bind:"{currentProduct.price}"
		},
		{
			xtype:"textfield",
			fieldLabel:"Category",
			bind:"{currentProduct.category}"
		}

	],
		bbar:[
				{
					xtype:"button",
					text:"Commit",
					listeners:{
						click:"commit"
				},
				bind:{
					disabled:"{!isRecordDirty}"
				}
				},
				{
					xtype:"button",
					text:"Reject",
					listellners:{
						click:"reject"
				}	
				}
				
				]
});