Ext.define("MyApp.view.ProductsTemplate",{

	extend:"Ext.Panel",
	alias:"widget.productstemplate",


/*bind:{	
		data : {
			bindTo: "{currentProduct}",
			deep: true        //to bind the updated data
		}
	 },

	//template
	tpl:[
			"<div>",
			"<p>Id: {id}</p>",
			"<p>Name: {name}</p>",
			"<p>Price: {price}</p>",
			"<p>Category: {category}</p>",
			"</div>"

	]*/

     bind:{

     	data: {
     		items: "{products}"
     	}
     },

     tpl: [
          '<div>',
              '<tpl for="items">',
                 '<div>',
                     '<p>Id: {data.id}</p>',
                     '<p>Name: {data.name}</p>',
                     '<p>Price: {data.price}</p>',
                     '<p>Category: {data.category}</p>',
                  '</div>',
               '</tpl>',
           '</div>'    

     ]

});