Ext.define("MyApp.product.ProductsGird",{

	extend:"Ext.grid.Panel",
	alias:"widget.productsgrid",
	header:{
		title:"Grid View",
		items:[
			{
				xtype:"button",
				text:"New",
				handler:"addNew"
			},
			{
				xtype:"button",
				text:"Commit All",
				handler:"commitAll"
			},
			{
				xtype:"button",
				text:"RejectAll",
				handler:"rejectAll"
			},
			{
				xtype:"button",
				text:"SaveAll",
				handler:"save"
			},
			{
				xtype:"textfield",
				emptyText:"Search",
				listeners:{
					change:"search"}
			}
			]
	},
	config:{
		currentProduct:null
	},
	bind:{
		//store: "{products}",
		currentProduct:"{currentProduct}"
	},
		publishes:["currentProduct"],
		//Publishes:value has changed to notify viewmodel
		listeners:{
			select:"onGridSelect",
			scope:"this"
		},

		onGridSelect:function(grid,record){
				this.setCurrentProduct(record);
		},
	columns:[
		{
			text:"Product Id",
			dataIndex:"id"

		},
		{
			text:"Name",
			dataIndex:"name"

		},
		{
			text:"Price",
			dataIndex:"price"

		},
		{
			text:"Category",
			dataIndex:"category"

		}
	]
});