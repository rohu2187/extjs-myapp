Ext.define("MyApp.view.products.ProductsContainer",{

	extend:"Ext.Panel",
	//xtype:"productscontainer",or
	alias:"widget.ProductsContainer",
	title:"Products",
	requires:["MyApp.view.products.ProductModel"],
	viewModel:{
			type:"productviewmodel"
				},
			controller:"productcontroller",

	items:[
		{
			xtype:"productsgrid",
			bind:{
				store: "{chainedStore1}"
			}		
		},
		{   
			xtype:"productsgrid",
			bind:{
				store: "{chainedStore2}"
			}		
		},
			{xtype:"productsform"},

			{xtype:"productstemplate"}
		],
		defaults:{
			margin: 8,
			border:true,
			bodypadding:10
		}
})