Ext.define('MyApp.ChartsDemo',{
	extend: "Ext.Panel",
	title: "Charts Demo",
	alias: "widget.chartsdemo",
	items: [
		{
			xtype: 'polar',
			height: 400,
			width: 400,

			store: {
				fields: ["cars","unitsSold"],
				data: [
					{cars: "Sedan", unitsSold: "450"},
					{cars: "SUV", unitsSold: "210"},
					{cars: "HetchBack", unitsSold: "760"},
					{cars: "Premium", unitsSold: "200"}
				]
			},
			series: {
				type: 'pie',
				highlight: true,
				angleField: 'unitsSold',
				label: {
					field: "cars"
				},
				donut: 20
			},
			legend: {
				field: "cars",
				docked: "bottom"
			}
		},

		{
			xtype: 'cartesian',
			height: 400,
			width: 700,

			store: {
				fields: ["cars","unitsSold"],
				data: [
					{cars: "Sedan", unitsSold: "450"},
					{cars: "SUV", unitsSold: "210"},
					{cars: "HetchBack", unitsSold: "760"},
					{cars: "Premium", unitsSold: "200"}
				]
			},
			series: {
				type: 'bar',
				highlight: true,
				xField: 'cars',
				yField: "unitsSold"

			},
			axes: [
				{
					type: 'numeric',
					position: 'left',
					title: {
						text: "Units Sold"
					},
					field: "unitsSold"
				},
				{
					type: 'category',
					position: 'bottom',
					title: {
						text: "Cars"
					},
					field:'cars'
				}
			]

		}
	]
})