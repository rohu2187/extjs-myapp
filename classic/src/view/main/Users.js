Ext.define("MyApp.view.main.Users",{

	extend:"Ext.grid.Panel",
	xtype:"userslist",
	title:"Users",
	requires:["MyApp.store.Users"],
	store:{
		type:"users"
	},
	columns:[
	        { 
	          dataIndex:"userName",
	          text:"Name"
        	},
		    { 
		       dataIndex:"designation",
	           text:"Designation"

			}

			]
});