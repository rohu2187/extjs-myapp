/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting automatically applies the "viewport"
 * plugin causing this view to become the body element (i.e., the viewport).
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('MyApp.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',

    requires: [
        'Ext.plugin.Viewport',
        'Ext.window.MessageBox',
        'Ext.chart.series.Pie',
        'Ext.chart.interactions.ItemHighlight',
        'Ext.chart.axis.Numeric',
        'Ext.chart.axis.Category',
        'Ext.chart.series.Bar',
        'Ext.chart.CartesianChart',
        'Ext.chart.PolarChart',

        'MyApp.view.main.MainController',
        'MyApp.view.main.MainModel',
        'MyApp.view.main.List',
        'MyApp.view.products.ProductsContainer',
        'MyApp.view.post.PostContainer',
        'MyApp.view.mvc.ProductsContainer',
        'MyApp.view.layouts.LayoutView',
        'MyApp.view.main.Users',
        'MyApp.view.products.ProductController',
        'MyApp.product.ProductsGird',
        'MyApp.view.product.ProductsForm',
        'MyApp.view.ProductsTemplate',
        'MyApp.product.PostGird',
        'MyApp.view.post.PostController',
        'MyApp.view.mvc.ProductsGird',
        'MyApp.view.mvc.ProductsForm',
        'MyApp.ChartsDemo',
        'MyApp.store.Posts'

    ],

    controller: 'main',
    viewModel: 'main',

    ui: 'navigation',

    tabBarHeaderPosition: 1,
    titleRotation: 0,
    tabRotation: 0,

    header: {
        layout: {
            align: 'stretchmax'
        },
        title: {
            bind: {
                text: '{name}'
            },
            flex: 0
        },
        iconCls: 'fa-th-list'
    },

    tabBar: {
        flex: 1,
        layout: {
            align: 'stretch',
            overflowHandler: 'none'
        }
    },

    responsiveConfig: {
        tall: {
            headerPosition: 'top'
        },
        wide: {
            headerPosition: 'left'
        }
    },

    defaults: {
        autoScroll: true,
        bodyPadding: 20,
        tabConfig: {
            plugins: 'responsive',
            responsiveConfig: {
                wide: {
                    iconAlign: 'left',
                    textAlign: 'left'
                },
                tall: {
                    iconAlign: 'top',
                    textAlign: 'center',
                    width: 120
                }
            },
            listeners:{
                click:"onTabClicked"
            }
        }
    },

    items: [{
        title: 'Home',
        iconCls: 'fa-home',
        // The following grid shares a store with the classic version's grid as well!
        items: [{
            xtype: 'mainlist'
        }],
        itemId:"home"

    }, {
        title: 'Users',
        iconCls: 'fa-user',
        /*bind: {
            html: '{loremIpsum}'
        },*/
        items:[
                {
                    xtype: "userslist"
                }
            ],
            itemId:"users"
    },
         {
        title: 'Products',
        iconCls: 'fa-users',
        items:[
                {
                    xtype: "ProductsContainer"
                }
            ],
            itemId:"products"
      },
         {
        title: 'Groups',
        iconCls: 'fa-users',
        bind: {
            html: '{loremIpsum}'
        }
    },
       {
        title: 'Post',
        iconCls: 'fa-users',
        items:[
                {
                    xtype: "PostContainer"
                }
            ],
            itemId:"post"
      },
      {
        title: 'MVC',
        iconCls: 'fa-users',
        items:[
                {
                    xtype: "mvcproductscontainer"
                }
            ],
            itemId:"mvc"
      },

      {
        title: 'Layout',
        iconCls: 'fa-cog',
        items:[
                {
                    xtype: "layoutview"
                }
            ],
            itemId:"layout"
       },
       {
        title: 'Charts',
        iconCls: 'fa-users',
        items:[
                {
                    xtype: "chartsdemo"
                }
            ],
            itemId:"charts"
      }
      ]
});
