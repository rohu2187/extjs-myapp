Ext.define("MyApp.view.layouts.LayoutView",{

extend:"Ext.Panel",
alias:"widget.layoutview",

   items:[

       {
       	xtype:"panel",
       	title:"Card Layout",
       	collapsible: true,
       	layout: "card",
       	minHeight: "300",
       	border:true,
       	margin:"10",
       	activeItem: 0,
       	items: [ 

	       	{
		       	xtype:"panel",
		       	title:"Panel 1",
		       	// html: "Panel #1"
		       	layout: {
			       	type:"hbox",
			       	pack:"center",
			       	align:"center"
		       	},
		       	bodyPadding: 10,
		       	defaults: {
		       		margin:"5 5 5 5",
		            border:true         
		       	},
		       	items:[
		            {xtype: "panel", title:"Hbox1", html:"Hbox1", minWidth: 150},
		            {xtype: "panel", title:"Hbox2", html:"Hbox2",minWidth: 150}
		       	]
	       	},

	       	{
		       	xtype:"panel",
		       	title:"Panel 2",
		       	layout:'accordion',
		       	defaults:{
		       		margin:"5 5 5 5",
	            	border:true
		       	},
		       	items:[
	                  {xtype: "panel", title:"Orders", html:"orders"},
	                  {xtype: "panel", title:"Customers", html:"Customers"}
	       		]	
	       	},

	       	{
		       	xtype:"panel",
		       	title:"Panel 3",
		       	html: "Panel #3"
	       	} 
       ],
       bbar:[
	       "->",
	       	{
		       	xtype:"button",
		       	text:"Previous",
		       	handler:function() {
	       			//console.log(this.getLayout());
					/*var layout = btn.up("panel").getLayout();
                    layout["prev"]();*/
                    var panel = this.up("panel");
                    if(panel.activeItem > 0){
                   		var index = --panel.activeItem;
                   		panel.getLayout().setActiveItem(index);
                   }

		       	}
	       	},
			{
		       	xtype:"button",
		       	text:"Next",
		       	handler:function() {
		       		/*var layout = btn.up("panel").getLayout();
					console.log( layout["next"]());
                    layout["next"]();*/
		       		var panel = this.up("panel");
                    if(panel.activeItem < 2){
                   		var index = ++panel.activeItem;
                   		panel.getLayout().setActiveItem(index);
                   }
		       	}
	       	}
       	]
       	
       }
      
   ]
});