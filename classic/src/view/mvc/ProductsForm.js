Ext.define("MyApp.view.mvc.ProductsForm",{

	extend:"Ext.form.Panel",
	alias:"widget.mvcproductsform",
	title:"Form ",
	
	items:[
		{
			xtype:"textfield",
			fieldLabel:"Id",
			name:"id"
			},
		{
			xtype:"textfield",
			fieldLabel:"Name",
			name:"name"
				},
		{
			xtype:"textfield",
			fieldLabel:"Price",
			name:"price"
		},
		{
			xtype:"textfield",
			fieldLabel:"Category",
			name:"category"
		}

	],
		bbar:[
		
				{
					xtype:"button",
					text:"Commit",
					listeners:{
						click:"commit"
				},
				bind:{
					disabled:"{!isRecordDirty}"
				}
				},
				{
					xtype:"button",
					text:"Reject"
					
				},{
					xtype:"button",
					text:"Update",
					action:"Update"
				}
				
				]
});