Ext.define("MyApp.view.mvc.ProductsContainer",{

	extend:"Ext.Panel",
	//xtype:"productscontainer",or
	alias:"widget.mvcproductscontainer",
	title:"Products",
	
	items:[
		{xtype:"mvcproductsgrid"},
		{xtype:"mvcproductsform"}

		],
		defaults:{
			margin: 8,
			border:true,
			bodypadding:10
		}
})