Ext.define("MyApp.view.mvc.ProductsGird",{

	extend:"Ext.grid.Panel",
	alias:"widget.mvcproductsgrid",
	store: "productstoreId",	
	header:{
		title:"Grid View",
	
	items:[ 
			{
				xtype:"button",
				text:"New",
				action:"New"
				
			},
			{
				xtype:"button",
				text:"Commit All"
				//action:"Commit"
				
			},
			{
				xtype:"button",
				text:"RejectAll"
				
			},
			{
				xtype:"button",
				text:"SaveAll"
				
			},
			{
				xtype:"textfield",
				emptyText:"Search"
			}	
	       ]
	},

	columns:[
		{
			text:"Product Id",
			dataIndex:"id"

		},
		{
			text:"Name",
			dataIndex:"name"

		},
		{
			text:"Price",
			dataIndex:"price"

		},
		{
			text:"Category",
			dataIndex:"category"

		}
	]
});