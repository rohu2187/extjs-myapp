Ext.define("MyApp.product.PostGird",{

	extend:"Ext.grid.Panel",
	alias:"widget.postgrid",
	header:{
		title:"Post Grid View"
	},
	controller:"postcontroller",
	
	bind:{
		store:"{post}"
	},
	plugins:["rowediting"],
	
	header:{
		items:[
			{

		xtype:"button",
		text:"save",
		handler:"save"

			}]

				},
	columns:[
		{
			text:"Id",			
			dataIndex:"id"

		},
		{
			text:"User Id",
			dataIndex:"userId"

		},
		{
			text:"Title",
			dataIndex:"title",
			flex: 1,
			editor:{
				xtype:"textfield",
				emptyText: "title"
			}

		},
		{
			text:"Body",
			dataIndex:"body",
			flex: 2,
			editor:{
				xtype:"textfield",
				emptyText: "body"
			}

		},
		{	xtype:"widgetcolumn",
		widget:{
			xtype:"button",
			text:"Remove",
			handler:"remove"
		}		
		}
	]
});